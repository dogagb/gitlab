# frozen_string_literal: true
# rubocop:disable Style/SignalException

CHANGED_FILES_MESSAGE = <<~MSG
For the following files, a review from the [Data team and Product Intelligence team](https://gitlab.com/groups/gitlab-org/growth/product-intelligence/engineers/-/group_members?with_inherited_permissions=exclude) is recommended
Please check the ~"product intelligence" [guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html) and reach out to %<engineers_group>s group for a review.


%<changed_files>s

MSG

UPDATE_METRICS_DEFINITIONS_MESSAGE = <<~MSG
  When adding, changing, or updating metrics, please update the [Event dictionary Usage Ping table](https://about.gitlab.com/handbook/product/product-analytics-guide#event-dictionary).

MSG

ENGINEERS_GROUP = '@gitlab-org/growth/product-intelligence/engineers'

UPDATE_DICTIONARY_MESSAGE = <<~MSG
  When updating metrics definitions, please update [Metrics Dictionary](https://docs.gitlab.com/ee/development/usage_ping/dictionary.html)

  ```shell
  bundle exec rake gitlab:usage_data:generate_metrics_dictionary
  ```
MSG

all_changed_files = helper.all_changed_files

tracking_files = [
  'lib/gitlab/tracking.rb',
  'spec/lib/gitlab/tracking_spec.rb',
  'app/helpers/tracking_helper.rb',
  'spec/helpers/tracking_helper_spec.rb',
  'app/assets/javascripts/tracking.js',
  'spec/frontend/tracking_spec.js',
  'generator_templates/usage_metric_definition/metric_definition.yml',
  'lib/generators/rails/usage_metric_definition_generator.rb',
  'spec/lib/generators/usage_metric_definition_generator_spec.rb',
  'config/metrics/schema.json'
]

tracking_changed_files   = all_changed_files & tracking_files
usage_data_changed_files = all_changed_files.grep(%r{(usage_data)})
metrics_changed_files    = all_changed_files.grep(%r{((ee/)?config/metrics/.*\.yml)})
dictionary_changed_file  = all_changed_files.grep(%r{(doc/development/usage_ping/dictionary.md)})

usage_changed_files = usage_data_changed_files + tracking_changed_files + metrics_changed_files + dictionary_changed_file

if usage_changed_files.any?

  mention = if helper.draft_mr?
              "`#{ENGINEERS_GROUP}`"
            else
              ENGINEERS_GROUP
            end

  warn format(CHANGED_FILES_MESSAGE, changed_files: helper.markdown_list(usage_changed_files), engineers_group: mention)
  warn format(UPDATE_METRICS_DEFINITIONS_MESSAGE) if usage_data_changed_files.any?

  fail format(UPDATE_DICTIONARY_MESSAGE) if metrics_changed_files.any? && dictionary_changed_file.empty?

  labels = ['product intelligence']
  labels << 'product intelligence::review pending' unless helper.mr_has_labels?('product intelligence::approved')

  markdown(helper.prepare_labels_for_mr(labels))
end
